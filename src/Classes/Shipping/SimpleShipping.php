<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:47
 */

namespace App\Classes\Shipping;


class SimpleShipping extends BaseShipping
{
    public function getCost() : int
    {
        return $this->shipping_entity_class->getCost();
    }
}