<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:40
 */

namespace App\Classes\Shipping;


abstract class BaseShipping implements Shipping
{
    /**
     * @var \App\Entity\Shipping $shipping_entity_class
     */
    protected $shipping_entity_class;

    abstract public function getCost() : int;

    public function __construct(\App\Entity\Shipping $shipping_entity)
    {
        $this->shipping_entity_class = $shipping_entity;
    }

    static function getHandlerClass($class_name = 'simple')
    {
        $shipping_class = "App\\Classes\\Shipping\\" . ucfirst(strtolower($class_name)) . 'Shipping';
        return new $shipping_class();
    }
}