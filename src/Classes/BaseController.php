<?php
/**
 * Created by PhpStorm.
 * User: mihalyfi
 * Date: 2018.09.15.
 * Time: 21:54
 */

namespace App\Classes;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    protected $categories;

    public function __construct()
    {

    }

    public function render(string $view, array $parameters = array(), Response $response = null): Response
    {
        $this->loadExtraData();
        $parameters['mainCategories'] = $this->mainCategories;
        return parent::render($view, $parameters, $response);
    }

    private function loadExtraData()
    {
        $this->mainCategories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findBy(array('parent' => null))
        ;
    }
}