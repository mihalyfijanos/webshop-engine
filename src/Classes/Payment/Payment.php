<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:42
 */

namespace App\Classes\Payment;


interface Payment
{
    public function getCost() : int;
}