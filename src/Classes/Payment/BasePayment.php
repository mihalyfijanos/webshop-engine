<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:40
 */

namespace App\Classes\Payment;


abstract class BasePayment implements Payment
{
    /**
     * @var \App\Entity\Payment $payment_entity_class
     */
    protected $payment_entity_class;

    abstract public function getCost() : int;

    public function __construct(\App\Entity\Payment $payment_entity)
    {
        $this->payment_entity_class = $payment_entity;
    }

    static function getHandlerClass($class_name = 'simple')
    {
        $payment_class = "App\\Classes\\Payment\\" . ucfirst(strtolower($class_name)) . 'Payment';
        return new $payment_class();
    }
}