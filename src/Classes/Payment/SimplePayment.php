<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:47
 */

namespace App\Classes\Payment;


class SimplePayment extends BasePayment
{
    public function getCost() : int
    {
        return $this->payment_entity_class->getCost();
    }
}