<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderProductRepository")
 */
class OrderProduct
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserOrder", inversedBy="orderProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $UserOrder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $OrderProductName;

    /**
     * @ORM\Column(type="integer")
     */
    private $OrderProductPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $OrderedQuantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserOrder(): ?UserOrder
    {
        return $this->UserOrder;
    }

    public function setUserOrder(?UserOrder $UserOrder): self
    {
        $this->UserOrder = $UserOrder;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(?Product $Product): self
    {
        $this->Product = $Product;

        return $this;
    }

    public function getOrderProductName(): ?string
    {
        return $this->OrderProductName;
    }

    public function setOrderProductName(string $OrderProductName): self
    {
        $this->OrderProductName = $OrderProductName;

        return $this;
    }

    public function getOrderProductPrice(): ?int
    {
        return $this->OrderProductPrice;
    }

    public function setOrderProductPrice(int $OrderProductPrice): self
    {
        $this->OrderProductPrice = $OrderProductPrice;

        return $this;
    }

    public function getOrderedQuantity(): ?int
    {
        return $this->OrderedQuantity;
    }

    public function setOrderedQuantity(int $OrderedQuantity): self
    {
        $this->OrderedQuantity = $OrderedQuantity;

        return $this;
    }

    public function increaseOrderedQuantity(int $increaseOrderedQuantity) :self
    {
        $this->OrderedQuantity += $increaseOrderedQuantity;

        return $this;
    }
}
