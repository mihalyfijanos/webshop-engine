<?php

namespace App\Controller\Admin;

use App\Classes\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;
use App\Form\CategoryType;

class CategoryController extends AdminBaseController
{
    /**
     * @Route("/admin/category", name="admin_category")
     */
    public function index()
    {
        return $this->render('admin/category/index.html.twig', [
            'controller_name' => 'CategoryController',
        ]);
    }

    /**
     * @Route("/admin/category/edit/{id}", name="admin_category_edit")
     */
    public function edit($id = null, Request $request) 
    {
        if ($id == null) {
            $category = new Category();
        } else {
            $category = $this->getDoctrine()->getRepository(Category::class)
            ->find($id);
        }
        $form = $this->createForm(CategoryType::class, $category);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            
            return $this->redirectToRoute('task_success');
        }
        return $this->render('admin/category/edit.html.twig', [
            'controller_name' => 'CategoryController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/category/new", name="admin_category_new")
     */
    public function new(Request $request)
    {
        return $this->edit(null, $request);
    }
}
