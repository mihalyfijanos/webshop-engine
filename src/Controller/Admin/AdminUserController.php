<?php

namespace App\Controller\Admin;

use App\Classes\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\AdminUser;
use App\Entity\AdminUserType;

class AdminUserController extends AdminBaseController
{
    /**
     * @Route("/admin/admin/user", name="admin_admin_user")
     */
    public function index()
    {
        return $this->render('admin/admin_user/index.html.twig', [
            'controller_name' => 'AdminUserController',
        ]);
    }

    /**
     * @Route("/admin/adminUser/edit/{id}", name="admin_adminuser_edit")
     */
    public function edit($id = null, Request $request) 
    {
        if ($id == null) {
            $adminuser = new AdminUser();
        } else {
            $adminuser = $this->getDoctrine()->getRepository(AdminUser::class)
            ->find($id);
        }
        $form = $this->createForm(AdminUserType::class, $adminuser);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $adminuser = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($adminuser);
            $entityManager->flush();
            
            return $this->redirectToRoute('task_success');
        }
        return $this->render('admin/admin_user/edit.html.twig', [
            'controller_name' => 'AdminUserController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/adminUser/new", name="admin_adminUser_new")
     */
    public function new(Request $request)
    {
        return $this->edit(null, $request);
    }
}
