<?php

namespace App\Controller\Admin;

use App\Classes\AdminBaseController;
use App\Entity\Sale;
use App\Form\SaleType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class SaleController extends AdminBaseController
{
    /**
     * @Route("/admin/sale", name="admin_sale")
     */
    public function index()
    {
        return $this->render('admin/sale/index.html.twig', [
            'controller_name' => 'SaleController',
        ]);
    }

	/**
	 * @Route("/admin/sale/edit/{id}", name="admin_sale_edit")
	 */
	public function edit($id = null, Request $request)
	{
		if ($id == null) {
			$sale = new Sale();
		} else {
			$sale = $this->getDoctrine()->getRepository(Sale::class)
				->find($id);
		}
		$form = $this->createForm(SaleType::class, $sale);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$sale = $form->getData();
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($sale);
			$entityManager->flush();

			return $this->redirectToRoute('task_success');
		}
		return $this->render('admin/sale/edit.html.twig', [
			'controller_name' => 'saleController',
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/admin/sale/new", name="admin_sale_new")
	 */
	public function new(Request $request)
	{
		return $this->edit(null, $request);
	}
}
