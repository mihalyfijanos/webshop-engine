<?php

namespace App\Controller\Admin;

use App\Classes\AdminBaseController;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AdminBaseController
{
    /**
     * @Route("/admin/user", name="admin_user")
     */
    public function index()
    {
        return $this->render('admin/user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/admin/user/edit/{id}", name="admin_user_edit")
     */
    public function edit($id = null, Request $request)
    {
        if ($id == null) {
            $user = new User();
        } else {
            $user = $this->getDoctrine()->getRepository(User::class)
                ->find($id);
        }
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('task_success');
        }
        return $this->render('admin/user/edit.html.twig', [
            'controller_name' => 'userController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/new", name="admin_user_new")
     */
    public function new(Request $request)
    {
        return $this->edit(null, $request);
    }
}
