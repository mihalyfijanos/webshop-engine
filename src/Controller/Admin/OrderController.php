<?php

namespace App\Controller\Admin;

use App\Classes\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\Order;
use App\Form\OrderType;

class OrderController extends AdminBaseController
{
    /**
     * @Route("/admin/order", name="admin_order")
     */
    public function index()
    {
        return $this->render('admin/order/index.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/admin/order/edit/{id}", name="admin_order_edit")
     */
    public function edit($id = null, Request $request) 
    {
        if ($id == null) {
            $order = new order();
        } else {
            $order = $this->getDoctrine()->getRepository(order::class)
            ->find($id);
        }
        $form = $this->createForm(orderType::class, $order);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();
            
            return $this->redirectToRoute('task_success');
        }
        return $this->render('admin/order/edit.html.twig', [
            'controller_name' => 'OrderController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/order/new", name="admin_order_new")
     */
    public function new(Request $request)
    {
        return $this->edit(null, $request);
    }
    
}
