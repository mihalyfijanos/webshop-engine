<?php

namespace App\Controller\Admin;

use App\Classes\AdminBaseController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Form\ProductType;

class ProductController extends AdminBaseController
{
    /**
     * @Route("/admin/product", name="admin_product")
     */
    public function index()
    {
        return $this->render('admin/product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }


    /**
     * @Route("/admin/product/edit/{id}", name="admin_product_edit")
     */
    public function edit($id = null, Request $request) 
    {
        if ($id == null) {
            $product = new product();
        } else {
            $product = $this->getDoctrine()->getRepository(product::class)
            ->find($id);
        }
        $form = $this->createForm(productType::class, $product);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
			$file = $product->getImage();

			$fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

			// Move the file to the directory where brochures are stored
			try {
				$file->move(
					$this->getParameter('product_images_directory'),
					$fileName
				);
			} catch (FileException $e) {
				// ... handle exception if something happens during file upload
			}

            $product = $form->getData();
			$product->setImage($fileName);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
            
            return $this->redirectToRoute('task_success');
        }
        return $this->render('admin/product/edit.html.twig', [
            'controller_name' => 'ProductController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/product/new", name="admin_product_new")
     */
    public function new(Request $request)
    {
        return $this->edit(null, $request);
    }
}
