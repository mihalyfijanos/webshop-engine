<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Classes\BaseController;

class CategoryController extends BaseController
{
    /**
     * @Route("/category", name="category")
     * 
     * Show all categories (only without parents)
     */
    public function index()
    {
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
        ]);
    }

    /**
     * @Route("/category/{id}", name="category_list")
     * 
     * Show selected categories child list
     */
    public function showChildCategories()
    {

    }
}
