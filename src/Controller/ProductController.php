<?php

namespace App\Controller;

use App\Classes\BaseController;
use App\Entity\Product;
use App\Entity\Category;
use App\Services\PriceService;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends BaseController
{
    /**
     * @Route("/product", name="product")
     * 
     * Show all products
     */
    public function index()
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }

    /**
     * @Route("/product/list", name="product_list")
     * @Route("/", name="index")
     */
    public function showAllProducts(PriceService $priceService)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();
        foreach ($products as &$product) {
        	$product->setPrice($priceService->getProductPrice($product));
		}

        return $this->render('product/list.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/product/byCategory/{category_id}", name="product_by_category")
     * 
     * Show all products in selected category
     */
    public function showProductsByCategory($category_id, PriceService $priceService)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($category_id)
            ;
        $products = $category->getProducts();
		foreach ($products as &$product) {
			$product->setPrice($priceService->getProductPrice($product));
		}
        return $this->render('product/list.html.twig', [
            'products' => $category->getProducts(),
        ]);
    }

    /**
     * @Route("/product/info/{product_id}", name="product_show")
     * 
     * Show product info page
     */
    public function showProductInfo($product_id, PriceService $priceService)
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($product_id);
        $product->setPrice($priceService->getProductPrice($product));
        return $this->render('product/product.html.twig', [
            'product' => $product
        ]);
    }
}
