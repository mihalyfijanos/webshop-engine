<?php

namespace App\Controller;

use App\Classes\BaseController;
use App\Form\ShippingSelectType;
use App\Services\OrderService;
use App\Services\ShippingService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShippingController extends BaseController
{
    /**
     * @Route("/cart/checkout/shipping", name="checkout_shipping")
     */
    public function index(ShippingService $shippingService, OrderService $orderService, Request $request)
    {
        $shippingTypes = $shippingService->getShippings();

        $cartOrder = $orderService->getCartOrder();

        $form = $this->createForm(ShippingSelectType::class, $cartOrder);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $cartOrder = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cartOrder);
            $entityManager->flush();

            return $this->redirectToRoute('checkout_payment');
        }
        return $this->render('shipping/select.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
