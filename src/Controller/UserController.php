<?php

namespace App\Controller;

use App\Classes\BaseController;
use App\Entity\User;
use App\Entity\UserData;
use App\Form\UserDataType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/user/data", name="user_data")
     *
     * Page to edit the user data
     */
    public function editUserData(Request $request)
    {
        $userData = $this->getDoctrine()
            ->getRepository(UserData::class)
            ->findBy(array('user' => $this->getUser()));
        if (empty($userData)) {
            $userData = new UserData();
            $userData->setUser($this->getUser());
        }
        $form = $this->createForm(UserDataType::class, reset($userData));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userData = $form->getData();
            $userData->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userData);
            $entityManager->flush();

            return $this->redirectToRoute('user_data');
        }
        return $this->render('user/edit.html.twig', [
            'controller_name' => 'UserController',
            'form' => $form->createView(),
        ]);
    }
}
