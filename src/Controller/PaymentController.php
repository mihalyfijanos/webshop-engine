<?php

namespace App\Controller;

use App\Classes\BaseController;
use App\Form\PaymentSelectType;
use App\Services\OrderService;
use App\Services\PaymentService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends BaseController
{
    /**
     * @Route("/cart/checkout/payment", name="checkout_payment")
     */
    public function index(PaymentService $paymentService, OrderService $orderService, Request $request)
    {
        $paymentTypes = $paymentService->getPayments();

        $cartOrder = $orderService->getCartOrder();

        $form = $this->createForm(PaymentSelectType::class, $cartOrder);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $cartOrder = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cartOrder);
            $entityManager->flush();

            return $this->redirectToRoute('checkout_cart');
        }
        return $this->render('payment/select.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
