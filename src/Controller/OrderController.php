<?php

namespace App\Controller;

use App\Classes\BaseController;
use App\Entity\Product;
use App\Services\PaymentService;
use App\Services\ShippingService;
use App\Services\UserService;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\OrderService;

class OrderController extends BaseController
{
    /**
     * @Route("/order", name="order")
     */
    public function index()
    {
        return $this->render('order/index.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function showCart(OrderService $orderService)
    {
        $cartOrder = $orderService->getCartOrder();
        return $this->render('order/cart.html.twig', [
            'controller_name' => 'OrderController',
            'cart_order' => $cartOrder,
        ]);
    }

    /**
     * @Route("/cart/addProduct/{product_id}", name="add_product_to_cart")
     */
    public function addProductToCart($product_id, OrderService $orderService)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($product_id);
        $orderService->addProductToCart($product);

        return $this->render('order/cart.html.twig', [
            'controller_name' => 'OrderController',
            'cart_order' => $orderService->getCartOrder(),
        ]);
    }

    /**
     * @Route("/cart/checkout", name="checkout_cart")
     */
    public function checkoutOrder(
    	OrderService $orderService,
		ShippingService $shippingService,
		PaymentService $paymentService,
		UserService $userService
	)
    {
    	if ($orderService->isCartEmpty()) {
    		return $this->redirectToRoute('cart');
		}

		if (!$userService->hasUserData($this->getUser())) {
			return $this->redirectToRoute('user_data');
		}

		if (!$shippingService->hasShipping($orderService->getCartOrder())) {
			var_dump("redirect to shipping");
			die();
			return $this->redirectToRoute('checkout_shipping');
		}

		if (!$paymentService->hasPayment($orderService->getCartOrder())) {
			return $this->redirectToRoute('checkout_payment');
		}

		$order_nr = $orderService->getOrderNrAndCheckout();
		return $this->render('order/checkout.html.twig', [
			'order_nr' => $order_nr,
		]);

    }
}
