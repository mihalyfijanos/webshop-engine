<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 10. 13.
 * Time: 11:46
 */

namespace App\Services;


use App\Entity\Product;

class PriceService
{
	public function getProductPrice(Product $product) {
		$price = $product->getPrice();
		if ($this->isOnSale($product)) {
			$price -= $product->getSale()->getValue();
		}
		return $price;
	}

	public function isOnSale($product) {
		return !($product->getSale() == null);
	}
}