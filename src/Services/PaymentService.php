<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:54
 */

namespace App\Services;


use App\Classes\Payment\BasePayment;
use App\Entity\UserOrder;
use App\Repository\PaymentRepository;

class PaymentService
{
    /**
     * @var PaymentRepository $payment_repository
     */
    private $payment_repository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->payment_repository = $paymentRepository;
    }

    public function getPaymentClassById($payment_id)
    {
        $payment_entity = $this->payment_repository->find($payment_id);
        $payment_class = BasePayment::getHandlerClass($payment_entity->getClass());
        return $payment_class;
    }

    public function getPayments()
    {
        return $this->payment_repository->findAll();
    }

	public function hasPayment(UserOrder $order)
	{
		return !($order->getPayment() == null);
	}
}