<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 09. 23.
 * Time: 12:54
 */

namespace App\Services;


use App\Classes\Shipping\BasePayment;
use App\Entity\UserOrder;
use App\Repository\ShippingRepository;

class ShippingService
{
    /**
     * @var ShippingRepository $shipping_repository
     */
    private $shipping_repository;

    public function __construct(ShippingRepository $shippingRepository)
    {
        $this->shipping_repository = $shippingRepository;
    }

    public function getShippingClassById($shipping_id)
    {
        $shipping_entity = $this->shipping_repository->find($shipping_id);
        $shipping_class = BasePayment::getHandlerClass($shipping_entity->getClass());
        return $shipping_class;
    }

    public function getShippings()
    {
        return $this->shipping_repository->findAll();
    }

    public function hasShipping(UserOrder $order)
	{
		return !($order->getShipping() == null);
	}
}