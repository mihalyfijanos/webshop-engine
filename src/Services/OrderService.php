<?php
namespace App\Services;

use App\Entity\OrderProduct;
use App\Entity\Product;
use App\Entity\UserOrder;
use App\Repository\OrderProductRepository;
use App\Repository\OrderRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OrderService
{
    public function __construct(
        TokenStorageInterface $tokenStorage,
        OrderRepository $orderRepository,
        OrderProductRepository $orderProductRepository,
		PriceService $priceService
    )
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->orderRepository = $orderRepository;
        $this->orderProductRepository = $orderProductRepository;
        $this->priceService = $priceService;
        $this->createCartOrderIfNotExists();
    }

    public function getCartOrder() : UserOrder
    {
        $this->createCartOrderIfNotExists();
        $orders = $this->orderRepository->findBy(array('user' => $this->user, 'status' => 0));
        return reset($orders);
    }

    public function isCartEmpty() : bool
    {
        return $this->getCartOrder()->getOrderProducts()->isEmpty();
    }

    public function addProductToCart(Product $product) :void
    {
    	$product->setPrice($this->priceService->getProductPrice($product));
        $orderProduct = $this->getExistingOrderProductOrCreateNew($product);
        $orderProduct->increaseOrderedQuantity(1);
        $this->orderProductRepository->saveOrderProduct($orderProduct);
    }

    public function getOrderNrAndCheckout() : int
    {
        $cart = $this->getCartOrder();
        $this->orderRepository->setCheckoutStatus($cart);
        return $cart->getId();
    }

    private function getExistingOrderProductOrCreateNew(Product $product) : OrderProduct
    {
        if ($this->isProductInCart($product)) {
            $orderProduct = $this->getOrderProductFromCart($product);
        } else {
            $orderProduct = $this->createOrderProductFromProduct($product);
        }
        return $orderProduct;
    }

    private function getOrderProductFromCart(Product $product) : ?OrderProduct{
        foreach ($this->getCartOrder()->getOrderProducts() as $orderProduct) {
            if ($orderProduct->getProduct()->getId() == $product->getId()) {
                return $orderProduct;
            }
        }
        return null;
    }

    private function isProductInCart(Product $product) : bool
    {
        $cart = $this->getCartOrder();
        foreach ($cart->getOrderProducts() as $orderProduct) {
            if ($orderProduct->getProduct()->getId() == $product->getId())
            {
                return true;
            }
        }
        return false;
    }

    private function createCartOrderIfNotExists() : void
    {
        if (!$this->isCartOrderExists()) {
            $this->orderRepository->createOrder($this->user, 0);
        }
    }

    private function isCartOrderExists() : bool
    {
        $orders = $this->orderRepository->findBy(array('user' => $this->user, 'status' => 0));
        return !empty($orders);
    }

    private function createOrderProductFromProduct(Product $product) :OrderProduct
    {
        $orderProduct = new OrderProduct();
        $orderProduct->setUserOrder($this->getCartOrder());
        $orderProduct->setOrderProductName($product->getName());
        $orderProduct->setOrderProductPrice($product->getPrice());
        $orderProduct->setOrderedQuantity(0);
        $orderProduct->setProduct($product);
        return $orderProduct;
    }
}