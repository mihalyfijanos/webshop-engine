<?php
/**
 * Created by PhpStorm.
 * User: mihalyfijanos
 * Date: 2018. 10. 12.
 * Time: 19:41
 */

namespace App\Services;


use App\Entity\User;

class UserService
{
	public function hasUserData(User $user)
	{
		return !($user->getUserData() == null);
	}

}