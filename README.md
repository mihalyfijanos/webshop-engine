# WEBSHOP ENGINE
## INSTALL
- Install php if it's not yet installed
- Install mysql server
- Clone git repo
- Edit .env file for your local mysql server
- Add admin user to admin_user table (use mysql client is a perfect way) and use this password: $2a$08$jHZj/wJfcVKlIwr5AvR78euJxYK7Ku5kURNhNx.7.CSIJ3Pq6LEPC
(it's admin)
- run composer requre --dev
- run php bin/console server:start

## ROUTES
- /admin/adminUser/edit/{id}
- /admin/adminUser/new
- /admin/category/edit/{id}
- /admin/category/new
- /admin/order/edit/{id}
- /admin/order/new
- /admin/product/edit/{id}
- /admin/product/new
- /category
- /category/{id}
- /product/list
- /product/byCategory/{id}
- /product/info/{id}

## FUNCTIONS TO DEVELOP
- Implement controller functions
- Implement cart (based on order, only this is the very first status) 
- Add checkout routes and address handling